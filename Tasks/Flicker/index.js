'use strict';
var angular = require('angular');
var app = angular.module('app', []);

app.controller('flickerCtrl', ['$scope', '$http', 'localStore', function ($scope, $http, localStore) {
	var savedData = localStore.getData();
	var messages = {
		noResult: "Sorry can't find any results",
		serverError  : "Hmm some errors on server ",
		loading: "Loading..."
	};

	// Initial state consist of
	// last search results and favorite photos
	$scope.photos = savedData.lastSearchResult;
	$scope.favoritePhotos = savedData.favoritePhotos;

	$scope.search = function () {
		$scope.message = messages.loading;

		$http({
			method: 'GET',
			url   : 'https://api.flickr.com/services/rest',
			params: {
				method        : 'flickr.photos.search',
				api_key       : '364bbc86e290a40519da176eea7b7df1',
				text          : $scope.query,
				per_page      : 9,
				format        : 'json',
				nojsoncallback: true
			}
		}).success(function (data) {
			$scope.photos = data.photos.photo;

			// Check if we have some results
			if( $scope.photos.length === 0 ) {
				$scope.message = messages.noResult;
			} else {
				$scope.message = '';
			}

			// Save new results to store
			localStore.saveLastSearchResult($scope.photos);

		}).error(function (error) {
			$scope.message = messages.serverError + error;
		});
	};

	// Toggle favorite photos sync with storage
	$scope.togglePhoto = function (target) {
		var favoritePhotos = localStore.checkFavoritePhotos(target.photo.id);

		$scope.favoritePhotos = favoritePhotos;
	};

}]);

app.factory('localStore', function () {
	var STORAGE_NAME = 'my-favorite-images';
	var defaultDataObject = {
		lastSearchResult: [],
		favoritePhotos  : {}
	};
	var localStore = {
		saveData: saveData,
		getData : getData,

		saveLastSearchResult: function (results) {
			var data = getData();
			data.lastSearchResult = results;
			saveData(data);
		},

		checkFavoritePhotos: function (photoId) {
			var data = getData();
			var favoritePhotos = data.favoritePhotos;
			var isFavoritePhoto = favoritePhotos[photoId];

			// Remove from favorite if it was there
			if( isFavoritePhoto ) {
				delete favoritePhotos[photoId];
			} else {
				favoritePhotos[photoId] = 'Like';
			}

			saveData(data);

			return favoritePhotos;
		},

		// Below all methods for debug;
		getFavoritePhotos: function () {
			console.warn('Favorite ', this.favoritePhotos);
			return this.favoritePhotos;
		},

		resetData: function () {
			console.warn('Clear store');
			localStorage.removeItem(STORAGE_NAME);
		},

		getLastSearchResult: function () {
			return getData().lastSearchResult;
		}

	};

	function getData() {
		var data = localStorage.getItem(STORAGE_NAME);
		return data === null ? defaultDataObject : JSON.parse(data);
	}

	function saveData(data) {
		localStorage.setItem(STORAGE_NAME, JSON.stringify(data));
	}

	return localStore;
});
