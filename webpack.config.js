'use strict';
var webpack = require('webpack');
var path = require('path');

module.exports = {
		context: path.join(__dirname, 'Tasks'),
		entry  : './Flicker/index.js',
		output : {
			path    : path.join(__dirname, 'Tasks/Flicker'),
			filename: 'bundle.js'
		},

		resolve: {
			modulesDirectories: ['node_modules'],
			extensions        : ['', '.js']
		},

		resolveLoader: {
			modulesDirectories: ['node_modules'],
			moduleTemplates   : ['*-loader', '*'],
			extensions        : ['', '.js']
		},

		module: {
			loaders: [
				{
					test  : /\.html$/,
					loader: 'raw'
				}
			]
		},

		plugins: [
			new webpack.HotModuleReplacementPlugin(),
			new webpack.NoErrorsPlugin()
		]
	};
