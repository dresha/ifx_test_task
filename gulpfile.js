'use strict';
// Design task
var gulp = require('gulp');
var sass = require('gulp-sass');
var neat = require('node-neat').includePaths;
var browserSync = require('browser-sync');
var open = require('gulp-open');
var webserver = require('gulp-webserver');
var paths = {
	scss: './Tasks/Design/sass/*.scss'
};

gulp.task('webserver', function() {
	gulp.src('Tasks')
		.pipe(webserver({
			port: 8080
		}));
});

gulp.task('allTask', ['taskDesign', 'taskFlicker']);

gulp.task('taskDesign', ['sass', 'webserver'], function(){
	gulp.src('./Tasks/Design/index.html')
		.pipe(open({uri: 'http://localhost:8080/Design/index.html'}));
});

gulp.task('taskFlicker', ['webserver'], function(){
	gulp.src('./Tasks/Flicker/index.html')
		.pipe(open({uri: 'http://localhost:8080/Flicker/index.html'}));
});

gulp.task('sass', function () {
	gulp.src('./Tasks/Design/sass/main.scss')
		.pipe(sass({
			includePaths: ['scss']
		}))
		.pipe(gulp.dest('Tasks/Design/style'));
});

gulp.task('browser-sync', function () {
	browserSync.init(['./Tasks/Design/style/*.css', 'js/*.js'], {
		server: './Tasks/Design'
	});
});

gulp.task('default', ['sass', 'browser-sync'], function () {
	gulp.watch(['./Tasks/Design/sass/*.scss', './Tasks/Design/sass/utils /*.scss', './Tasks/Design/sass/base/*.scss', './Tasks/Design/sass/layout/*.scss', './Tasks/Design/sass/module/*.scss', './Tasks/Design/sass/state/*.scss'], ['sass']);
});